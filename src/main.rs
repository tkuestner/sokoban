use bevy::prelude::*;
use bevy_egui::{egui, EguiContext, EguiPlugin};
use bevy_tweening::lens::TransformPositionLens;
use bevy_tweening::TweenCompleted;
use bevy_tweening::*;
use clap::Parser;
use std::time::Duration;

use sokoban::coord_vec::Direction;
use sokoban::game::*;
use sokoban::level::*;

const TILE_SIZE: f32 = 32.0; // cf. sprite sheet

/// Simple Sokoban clone made with bevy and egui
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Path to the Sokoban level file (*.slc)
    #[clap(short, long)]
    file_path: String,
}

#[derive(Component)]
struct Player {}

impl Player {
    fn new() -> Self {
        Player {}
    }
}

#[derive(Debug, Component)]
struct Pallet {
    id: u32,
}

impl Pallet {
    fn new(id: u32) -> Self {
        Pallet { id }
    }
}

fn main() {
    let args = Args::parse();

    let starting_position = LevelCollection::from_file(&args.file_path)
        .expect("Cannot open Sokoban level file")
        .levels
        .get(0)
        .expect("The Sokoban level file does not contain any levels")
        .starting_position
        .clone();

    App::new()
        .insert_resource(WindowDescriptor {
            title: "Sokoban".to_string(),
            width: 640.0,
            height: 480.0,
            ..Default::default()
        })
        .insert_resource(ClearColor(Color::rgb(0.13, 0.13, 0.13)))
        .insert_resource(Game::new(starting_position))
        .add_plugins(DefaultPlugins)
        .add_plugin(TweeningPlugin)
        .add_plugin(EguiPlugin)
        .add_startup_system(setup)
        .add_system(ui)
        .add_system(player_movement_system)
        .add_system(animation_completed_system)
        .run();
}

fn ui(mut egui_context: ResMut<EguiContext>, game: Res<Game>) {
    egui::TopBottomPanel::bottom("bottom_panel")
        .resizable(false)
        .show(egui_context.ctx_mut(), |ui| {
            ui.with_layout(egui::Layout::left_to_right(), |ui| {
                ui.label(format!("Moves: {}", game.moves()));
                ui.label(format!("Pushes: {}", game.pushes()));
            });
        });
}

fn setup(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
    game: Res<Game>,
) {
    let texture_handle = asset_server.load("spritesheet.png");
    let texture_atlas = TextureAtlas::from_grid_with_padding(
        texture_handle,
        Vec2::new(32.0, 32.0),
        4,
        1,
        Vec2::new(2.0, 0.0),
    );
    let texture_atlas_handle = texture_atlases.add(texture_atlas);

    commands.spawn_bundle(OrthographicCameraBundle::new_2d());

    for &wall in &game.position().walls {
        commands.spawn().insert_bundle(SpriteSheetBundle {
            texture_atlas: texture_atlas_handle.clone(),
            transform: grid_transform(wall, game.position()),
            sprite: TextureAtlasSprite::new(0),
            ..Default::default()
        });
    }

    for &target in &game.position().targets {
        commands.spawn().insert_bundle(SpriteSheetBundle {
            texture_atlas: texture_atlas_handle.clone(),
            transform: grid_transform(target, game.position()),
            sprite: TextureAtlasSprite::new(1),
            ..Default::default()
        });
    }

    for (&pallet_index, &pallet_coord) in &game.position().pallets {
        commands
            .spawn()
            .insert_bundle(SpriteSheetBundle {
                texture_atlas: texture_atlas_handle.clone(),
                transform: grid_transform(pallet_coord, game.position()),
                sprite: TextureAtlasSprite::new(2),
                ..Default::default()
            })
            .insert(Pallet::new(pallet_index));
    }

    commands
        .spawn()
        .insert_bundle(SpriteSheetBundle {
            texture_atlas: texture_atlas_handle,
            transform: grid_transform(game.position().player, game.position()),
            sprite: TextureAtlasSprite::new(3),
            ..Default::default()
        })
        .insert(Player::new());
}

fn direction_from_keyboard(keyboard_input: Res<Input<KeyCode>>) -> Option<Direction> {
    let pressed_keys: Vec<KeyCode> = [KeyCode::Up, KeyCode::Down, KeyCode::Left, KeyCode::Right]
        .into_iter()
        .filter(|&keycode| keyboard_input.pressed(keycode))
        .collect();

    if let [keycode] = pressed_keys.as_slice() {
        match keycode {
            KeyCode::Up => Some(Direction::Up),
            KeyCode::Down => Some(Direction::Down),
            KeyCode::Left => Some(Direction::Left),
            KeyCode::Right => Some(Direction::Right),
            _ => None,
        }
    } else {
        None
    }
}

fn player_movement_system(
    keyboard_input: Res<Input<KeyCode>>,
    mut player_query: Query<
        (
            Entity,
            &mut Player,
            &mut Transform,
            Option<&Animator<Transform>>,
        ),
        Without<Pallet>,
    >,
    mut pallet_query: Query<(Entity, &mut Pallet, &mut Transform), Without<Player>>,
    mut game: ResMut<Game>,
    mut commands: Commands,
) {
    let (player_entity, _player, transform, animator) = player_query.single_mut();

    // Do not accept keyboard input if an animation is still running (i.e. the player is in-between
    // squares)
    // The following code does not work. Return is called "too often", i.e. the player sprite
    // "stumbles", halts for a short time on some squares. This probably has to do with the order
    // of scheduling of the systems (player_movement_system, animation system, animation_completed_system)
    if let Some(a) = animator {
        if let Some(tweenable) = a.tweenable() {
            if tweenable.progress() < 1.0 {
                return;
            }
        }
    }
    // Due to parallel execution of the systems, it can happen that the player_movement_system
    // is executed before the animation_completed_system. The player will then rest on a square
    // for one or two frames more than necessary. This looks like stumbling.
    if game.action_in_flight().is_some() {
        return;
    }

    if let Some(direction) = direction_from_keyboard(keyboard_input) {
        if let Some(action) = game.input(direction) {
            const DURATION: u64 = 320;

            // Animate player
            let target_square = action.next_position.player;
            let tween = Tween::new(
                EaseMethod::Linear,
                TweeningType::Once,
                Duration::from_millis(DURATION),
                TransformPositionLens {
                    start: transform.translation,
                    end: grid_to_world(target_square, &action.next_position),
                },
            )
            .with_completed_event(true, 0);
            commands.entity(player_entity).insert(Animator::new(tween));

            if let Action::Push { pallet_id } = action.action {
                // Find the correct entity with this pallet_id
                for (entity, pallet, transform) in pallet_query.iter_mut() {
                    if pallet.id != pallet_id {
                        continue;
                    }

                    let target_square = action
                        .next_position
                        .pallets
                        .get_by_left(&pallet_id)
                        .unwrap();
                    let tween = Tween::new(
                        EaseMethod::Linear,
                        TweeningType::Once,
                        Duration::from_millis(DURATION),
                        TransformPositionLens {
                            start: transform.translation,
                            end: grid_to_world(*target_square, &action.next_position),
                        },
                    );
                    commands.entity(entity).insert(Animator::new(tween));
                }
            }
        }
    }
}

fn animation_completed_system(mut reader: EventReader<TweenCompleted>, mut game: ResMut<Game>) {
    for _event in reader.iter() {
        game.commit_action();
    }
}

fn grid_to_world(coordinate: Coord, position: &Position) -> Vec3 {
    // Flip y axis, scale by sprite size and center at (0, 0) (which is the center of the window)
    // TODO Maybe make z a parameter or maybe compose from bevy::Transform
    let x = coordinate.x as f32;
    let y = coordinate.y as f32;
    let width = position.width as f32;
    let height = position.height as f32;
    Vec3::new(
        (x - (width - 1.0) / 2.0) * TILE_SIZE,
        ((height - y) - (height - 1.0) / 2.0) * TILE_SIZE,
        1.0,
    )
}

fn grid_transform(coordinate: Coord, position: &Position) -> Transform {
    Transform::from_translation(grid_to_world(coordinate, position))
}
