use crate::coord_vec::Direction;
use crate::level::*;

#[derive(Debug)]
pub struct ActionInFlight {
    pub action: Action,
    pub next_position: Position,
}

pub struct Game {
    moves: u32,
    pushes: u32,

    positions: Vec<Position>,
    action_in_flight: Option<ActionInFlight>,
}

impl Game {
    pub fn new(starting_position: Position) -> Self {
        Game {
            moves: 0,
            pushes: 0,
            positions: vec![starting_position],
            action_in_flight: None,
        }
    }

    pub fn moves(&self) -> u32 {
        self.moves
    }

    pub fn pushes(&self) -> u32 {
        self.pushes
    }

    pub fn position(&self) -> &Position {
        self.positions.last().unwrap()
    }

    pub fn action_in_flight(&self) -> Option<&ActionInFlight> {
        self.action_in_flight.as_ref()
    }

    pub fn input(&mut self, direction: Direction) -> Option<&ActionInFlight> {
        if self.action_in_flight.is_some() {
            return None;
        }
        let mut position = self.positions.last().unwrap().clone();
        let action = position.input(direction);
        if matches!(action, Action::None) {
            return None;
        }
        self.action_in_flight = Some(ActionInFlight {
            action,
            next_position: position,
        });

        self.action_in_flight.as_ref()
    }

    pub fn commit_action(&mut self) {
        if let Some(action_in_flight) = &self.action_in_flight {
            match action_in_flight.action {
                Action::Move => self.moves += 1,
                Action::Push { .. } => {
                    self.moves += 1;
                    self.pushes += 1
                }
                Action::None => (),
            }
            self.positions.push(action_in_flight.next_position.clone());
        }
        self.action_in_flight = None;
    }
}
