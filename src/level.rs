use bimap::{BiMap, Overwritten};
use roxmltree::{Document, Node};
use std::collections::HashSet;
use std::{fmt, fs, io};

use crate::coord_vec::{CoordVec, Direction};

pub type Coord = CoordVec<i32>;

#[derive(Debug, Default)]
pub struct LevelCollection {
    pub title: String, // TODO accessors
    pub description: String,
    pub email: String,
    pub url: String,
    pub copyright: String,
    pub levels: Vec<Level>,
}

#[derive(Debug)]
pub struct Level {
    //copyright: String,  // present in SokobanLev.xsd but it seems not to be used in any *.slc file
    pub name: String, // 'id' in SokobanLev.xsd but decided to rename it here
    pub starting_position: Position,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Position {
    pub width: usize,  // TODO accessor instead of pub
    pub height: usize, // maybe type should u32 (big enough)
    pub walls: HashSet<Coord>,
    pub targets: HashSet<Coord>,
    pub pallets: BiMap<u32, Coord>, // TODO rename pallets
    pub player: Coord,
}

#[derive(Debug, Copy, Clone)]
pub enum Action {
    None,
    Move,
    Push { pallet_id: u32 },
}

#[derive(Debug)]
pub struct ReadFileError {}

impl LevelCollection {
    pub fn from_file(filepath: &str) -> Result<LevelCollection, ReadFileError> {
        let data = fs::read_to_string(filepath)?;
        // Roxmltree can only parse UTF-8 input and we expect every *.slc file to be UTF-8 encoded
        let doc = Document::parse(&data)?;

        let root_node = doc
            .descendants()
            .find(|n| n.tag_name().name() == "SokobanLevels")
            .ok_or(ReadFileError {})?;

        let title = text_by_tag_or_default(&root_node, "Title")
            .trim()
            .to_string();
        let description = text_by_tag_or_default(&root_node, "Description")
            .trim()
            .to_string();
        let email = text_by_tag_or_default(&root_node, "Email")
            .trim()
            .to_string();
        let url = text_by_tag_or_default(&root_node, "Url").trim().to_string();

        let collection_node = root_node
            .children()
            .find(|n| n.tag_name().name() == "LevelCollection")
            .ok_or(ReadFileError {})?;

        let copyright = collection_node
            .attribute("Copyright")
            .unwrap_or("")
            .trim()
            .to_string();

        let mut levels = vec![];

        for level_node in collection_node
            .children()
            .filter(|n| n.tag_name().name() == "Level")
        {
            let id = level_node.attribute("Id").unwrap_or("").trim();

            let mut level_string = "".to_string();
            for line_node in level_node.children().filter(|n| n.tag_name().name() == "L") {
                if let Some(l) = line_node.text() {
                    level_string.push_str(l);
                    level_string.push('\n');
                }
            }

            levels.push(Level {
                name: id.to_string(),
                starting_position: Position::from_level_string(&level_string)?,
            });
        }

        Ok(LevelCollection {
            title,
            description,
            email,
            url,
            copyright,
            levels,
        })
    }
}

impl Position {
    fn from_level_string(s: &str) -> Result<Self, ReadFileError> {
        // Data structures in struct Position
        let mut walls = HashSet::new();
        let mut targets = HashSet::new();
        let mut pallets = BiMap::new();
        let mut players = vec![];

        // Read line-by-line, skip whitespace-empty lines
        // Count maximum rows (lines) and maximum columns
        let mut max_column = 0;
        let mut max_row = 0;
        let mut pallet_counter = 0;

        for (row, line) in s.lines().enumerate() {
            let line = line.trim_end();
            if line.is_empty() {
                continue;
            }
            max_row = std::cmp::max(max_row, row);
            for (column, character) in line.chars().enumerate() {
                max_column = std::cmp::max(max_column, column);
                // TODO Bail if level is too big
                //let coord = Coord{ x: column as i32, y: row as i32 };
                let coord = Coord::new(column as i32, row as i32);
                match character {
                    ' ' => (), // space, empty square
                    '.' => {
                        targets.insert(coord);
                    } // target square (storage location)
                    '*' => {
                        // pallet on target square
                        pallets.insert(pallet_counter, coord);
                        pallet_counter += 1;
                        targets.insert(coord);
                    }
                    '$' => {
                        // pallet
                        pallets.insert(pallet_counter, coord);
                        pallet_counter += 1;
                    }
                    '+' => {
                        // player on target square
                        players.push(coord);
                        targets.insert(coord);
                    }
                    '@' => {
                        players.push(coord);
                    } // player character
                    '#' => {
                        walls.insert(coord);
                    } // wall
                    _ => (), // TODO error, unknown character
                }
            }
        }

        if players.len() != 1 {
            return Err(ReadFileError {});
        }
        let player = players[0];

        Ok(Position {
            width: max_column + 1,
            height: max_row + 1,
            walls,
            targets,
            pallets,
            player,
        })
    }

    fn valid_square(&self, coord: Coord) -> bool {
        (0 <= coord.x)
            && (coord.x < self.width as i32)
            && (0 <= coord.y)
            && (coord.y < self.height as i32)
    }

    fn has_pallet(&self, coord: Coord) -> bool {
        self.pallets.contains_right(&coord)
    }

    fn has_wall(&self, coord: Coord) -> bool {
        self.walls.contains(&coord)
    }

    fn is_blocked(&self, coord: Coord) -> bool {
        self.has_pallet(coord) || self.has_wall(coord)
    }

    pub fn input(&mut self, direction: Direction) -> Action {
        let player_target_square = self.player + direction.as_coord();
        if !self.valid_square(player_target_square) {
            return Action::None;
        }

        if !self.is_blocked(player_target_square) {
            self.player = player_target_square;
            return Action::Move;
        }

        let pallet_on_target = self.pallets.get_by_right(&player_target_square);
        if let Some(&pallet_id) = pallet_on_target {
            let pallet_target_square = player_target_square + direction.as_coord();
            if !self.is_blocked(pallet_target_square) {
                self.pallets.remove_by_left(&pallet_id);
                assert!(
                    matches!(
                        self.pallets.insert(pallet_id, pallet_target_square),
                        Overwritten::Neither
                    ),
                    "Internal game state inconsistent"
                );
                self.player = player_target_square;
                return Action::Push { pallet_id };
            }
        }

        Action::None
    }
}

impl fmt::Display for Position {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        //write!(f, "({}, {})", self.x, self.y)

        // Build empty output string
        let mut output: Vec<String> = vec![];
        for r in 0..self.height {
            let mut line = String::new();
            for c in 0..self.width {
                let coord = Coord {
                    x: c as i32,
                    y: r as i32,
                };
                let is_wall: bool = self.walls.contains(&coord);
                let is_target: bool = self.targets.contains(&coord);
                let is_pallet: bool = self.pallets.contains_right(&coord);
                let is_player: bool = self.player == coord;
                // (In a way, this is a bitfield of 4 bits, 7 combinations are valid, i.e.
                // they have an equivalent character representation, 9 are invalid.)
                // This is not the right time to do internal consistency checks.

                if is_target && is_pallet {
                    line.push('*');
                    continue;
                }

                if is_target && is_player {
                    line.push('+');
                    continue;
                }

                if is_wall {
                    line.push('#');
                    continue;
                }

                if is_target {
                    line.push('.');
                    continue;
                }

                if is_pallet {
                    line.push('$');
                    continue;
                }

                if is_player {
                    line.push('@');
                    continue;
                }

                line.push(' ');
            }
            output.push(line.trim_end().to_string());
        }

        write!(f, "{}", output.join("\n"))
    }
}

impl From<io::Error> for ReadFileError {
    fn from(_: io::Error) -> Self {
        ReadFileError {}
    }
}

impl From<roxmltree::Error> for ReadFileError {
    fn from(_: roxmltree::Error) -> Self {
        ReadFileError {}
    }
}

/// Finds a child node by tag name and returns its text. Returns an empty string in case of failure.
///
/// # Examples
///
/// ```
///    let parent = roxmltree::Document::parse("<r><p> text</p></r>").unwrap().root_element();
///    assert_eq!(text_by_tag_or_default(&parent, "p"), " text");
/// ```
fn text_by_tag_or_default<'a>(node: &'a Node, tag: &str) -> &'a str {
    let child_node = node.children().find(|n| n.tag_name().name() == tag);
    match child_node {
        Some(element) => match element.text() {
            Some(t) => t,
            _ => "",
        },
        _ => "",
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::path::PathBuf;

    #[test]
    fn level_collection_from_file() {
        let mut path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        path.push("tests/resources/Example.slc");
        let collection = LevelCollection::from_file(path.to_str().unwrap()).unwrap();

        assert_eq!(collection.title, "Example collection");
        assert_eq!(collection.description, "Example collection for unit tests.");
        assert_eq!(collection.email, "example@example.com");
        assert_eq!(collection.url, "https://www.example.com");
        assert_eq!(collection.copyright, "John Doe");

        assert_eq!(collection.levels.len(), 61);
        assert_eq!(collection.levels[0].name, "#1");
        assert_eq!(collection.levels[60].name, "#61");

        let expected_level_string = r#"    #####
    #   #
    #   #
  ###   ##
  #      #
### # ## #   ######
#   # ## #####    #
# $  $           .#
#####$### #@##  ..#
    #     #########
    #######"#;
        assert_eq!(
            format!("{}", collection.levels[0].starting_position),
            expected_level_string
        );
    }

    #[test]
    fn position() {
        let example_position = r#"#####
#@$.#
#####"#;
        let position1 = Position::from_level_string(example_position).unwrap();
        let output = format!("{}", position1);
        let position2 = Position::from_level_string(&output).unwrap();
        assert_eq!(position1, position2);

        let multiple_players_position = r#"#####
#@@.#
#####"#;
        assert!(Position::from_level_string(multiple_players_position).is_err());

        let complex_example_position = r#"#####
#+*$#
#####"#;
        let position = Position::from_level_string(complex_example_position).unwrap();
        assert!(position.has_pallet(Coord { x: 2, y: 1 }));
        assert!(position.has_pallet(Coord { x: 3, y: 1 }));
        assert!(position.valid_square(Coord { x: 0, y: 0 }));
        assert!(position.has_wall(Coord { x: 0, y: 0 }));
        assert!(position.is_blocked(Coord { x: 0, y: 0 }));
    }

    #[test]
    fn input() {
        let example_position = r#"#####
#@$.#
#####"#;
        let mut position = Position::from_level_string(example_position).unwrap();
        let action = position.input(Direction::Right);
        assert!(matches!(action, Action::Push{ pallet_id: 0}));
        assert_eq!(position.player, Coord { x: 2, y: 1 });
        assert!(position.has_pallet(Coord { x: 3, y: 1 }));
    }

    #[test]
    fn text_by_tag_or_default() {
        let doc = roxmltree::Document::parse("<r><p> text</p></r>").unwrap();
        let root_element = doc.root_element();
        assert_eq!(super::text_by_tag_or_default(&root_element, "p"), " text");
        assert_eq!(super::text_by_tag_or_default(&root_element, "q"), "");

        let doc = roxmltree::Document::parse("<r><p/></r>").unwrap();
        let root_element = doc.root_element();
        assert_eq!(super::text_by_tag_or_default(&root_element, "p"), "");
    }
}
