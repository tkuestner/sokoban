use std::ops::{Add, AddAssign, Mul, Neg};

#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub struct CoordVec<T> {
    pub x: T,
    pub y: T,
}

#[derive(Copy, Clone, Debug)]
pub enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Direction {
    pub(crate) const fn as_coord(self) -> CoordVec<i32> {
        use Direction::*;
        match self {
            Up => CoordVec { x: 0, y: -1 },
            Down => CoordVec { x: 0, y: 1 },
            Left => CoordVec { x: -1, y: 0 },
            Right => CoordVec { x: 1, y: 0 },
        }
    }
}

impl<T> CoordVec<T> {
    pub fn new(x: T, y: T) -> Self {
        Self { x, y }
    }
}

impl<T, U> Neg for CoordVec<T>
where
    T: Neg<Output = U>,
{
    type Output = CoordVec<U>;
    fn neg(self) -> Self::Output {
        Self::Output {
            x: -self.x,
            y: -self.y,
        }
    }
}

impl<T, U, V> CoordVec<T>
where
    T: Mul<T, Output = U> + Copy,
    U: Add<U, Output = V>,
{
    pub fn dot(lhs: Self, rhs: Self) -> V {
        lhs.x * rhs.x + lhs.y * rhs.y
    }

    pub fn norm_squared(self) -> V {
        self.x * self.x + self.y * self.y
    }
}

// Debatable
impl<T> CoordVec<T> {
    pub fn magnitude<U, V>(&self) -> f64
    where
        T: Mul<T, Output = U> + Copy,
        U: Add<U, Output = V>,
        V: Into<f64>,
    {
        let norm = self.norm_squared();
        f64::sqrt(norm.into())
    }
}

impl<T: Add<Output = T>> Add for CoordVec<T> {
    type Output = CoordVec<T>;
    fn add(self, rhs: CoordVec<T>) -> Self::Output {
        CoordVec {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl<T: AddAssign> AddAssign for CoordVec<T> {
    fn add_assign(&mut self, rhs: Self) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn creation() {
        let v1 = CoordVec::new(1, 2);
        assert_eq!(v1.x, 1);
        assert_eq!(v1.y, 2);

        let v2 = CoordVec::new(3.3, 4.4);
        assert_eq!(v2.x, 3.3);
        assert_eq!(v2.y, 4.4);
    }

    #[test]
    fn negation() {
        let v1 = CoordVec::new(1, 2);
        let v2 = -v1;
        assert_eq!(v2, CoordVec { x: -1, y: -2 });
    }

    #[test]
    fn dot_product() {
        let v1 = CoordVec::new(1, 2);
        let v2 = CoordVec::new(3, 4);
        assert_eq!(CoordVec::dot(v1, v2), 11);
    }

    #[test]
    fn norm_squared() {
        let v = CoordVec::new(-2, 3);
        assert_eq!(v.norm_squared(), 13);
    }

    #[test]
    fn magnitude() {
        let v1 = CoordVec::new(-3.0, 4.0);
        assert_eq!(v1.magnitude(), 5.0);

        let v2 = CoordVec::new(-3, 4);
        assert_eq!(v2.magnitude(), 5.0);
    }

    #[test]
    fn add() {
        let v1 = CoordVec::new(1, 2);
        let v2 = CoordVec::new(3, 4);
        assert_eq!(v1 + v2, CoordVec { x: 4, y: 6 });
        assert_eq!(v2 + v1, CoordVec { x: 4, y: 6 });

        let mut v3 = CoordVec::new(5, 6);
        v3 += v2;
        assert_eq!(v3, CoordVec { x: 8, y: 10 });
    }

    #[test]
    fn direction() {
        assert_eq!(Direction::Up.as_coord(), CoordVec { x: 0, y: -1 });
        assert_eq!(Direction::Down.as_coord(), CoordVec { x: 0, y: 1 });
        assert_eq!(Direction::Left.as_coord(), CoordVec { x: -1, y: 0 });
        assert_eq!(Direction::Right.as_coord(), CoordVec { x: 1, y: 0 });
    }
}
